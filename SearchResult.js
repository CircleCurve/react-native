import React, { Component } from 'react';
import {
    StyleSheet,
    Image,
    View,
    TouchableHighlight,
    ListView,
    Text
} from 'react-native';

import {PropertyView} from './PropertyView';

export class SearchResult extends Component {
    static navigationOptions = {
        title: "Search Result",
    };
    
    constructor(props) {
        super(props);
        console.log("Come inside search props") ; 
        this.params = this.props.navigation.state.params;  
        const dataSource = new ListView.DataSource({
            rowHasChanged: (r1, r2) =>  r1.lister_url !== r2.lister_url
        });

        this.state = {
            dataSource: dataSource.cloneWithRows(this.params.listings)
        };
        //console.log( params.listings); 
    }
    rowPressed(listerURL) {
        
        let property = this.params.listings.filter(prop => 
            prop.lister_url === listerURL
        )[0]; 

        this.props.navigation.navigate('propertyView' , {
            property : property
        });
    }

    renderRow(rowData, sectionId, rowId) {
        return (
            <TouchableHighlight
                onPress={() => this.rowPressed(rowData.lister_url)}
                underlayColor='#dddddd'>
                <View style={styles.rowContainer}>
                    <Image style={styles.thumb} source={{uri : rowData.img_url}} />
                    <View style={styles.textContainer}>
                        <Text style={styles.title}>{rowData.title} </Text>
                        <Text style={styles.price}>{rowData.price_formatted} </Text>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }

    render() {
        return (
            <ListView
                dataSource={this.state.dataSource}
                renderRow={this.renderRow.bind(this)}
                />
        );
    }

}

const styles = StyleSheet.create({
    thumb : {
        width : 80,
        height : 80,
        marginRight : 10
    },
    textContainer : {
        flex : 1,
    },
    seperator : {
        height : 1 ,
        backgroundColor : "#dddddd"
    },
    price : {
        fontSize :25, 
        fontWeight : "bold",
        color : "#48BBEC",
        alignSelf: 'flex-end',
    },
    title : {
        fontSize : 20, 
        color : "#656565"
    },
    rowContainer : {
        flexDirection : 'row',
        padding : 10
    }
});