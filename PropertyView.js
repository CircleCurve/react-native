'use strict';

import React, { Component } from 'react'
import {
  StyleSheet,
  Image,
  View,
  Text
} from 'react-native';



export class PropertyView extends Component {
    static navigationOptions = {
        title: "Property",
    };

    constructor(props) {
        super(props); 
        this.params = this.props.navigation.state.params; 
    }

    render () {
 
        let property = this.params.property; 
        let stats = property.bedroom_number + ' bed' + property.property_type;
        if (property.bathroom_number){
            stats += ', ' + property.bathroom_number + ' '+ 
            (property.bathroom_number > 1 ? 'bathrooms' : 'bathroom'); 
        } 
        let price = property.price_formatted.split(' ')[0]; 
        return (
            <View style={styles.container}>
                <Image style={styles.image} 
                    source={{uri : property.img_url }}
                />
                <View style={styles.heading}>
                    <Text style={styles.price}>{price}</Text>
                    <Text style={styles.title}>{property.title}</Text>
                    <View style={styles.seperator} />
                </View>
                <Text style={styles.description}>{stats}</Text>
                <Text style={styles.description}>{property.summary}</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container : {
        flex:  1 
    },
    heading : {
        backgroundColor : '#F8F8F8'
    },
    seperator : {
        height : 1, 
        backgroundColor : '#DDDDDD'
    }, 
    image : {
        //width : 400 , 
        height : 300
    },
    price : {
        fontSize: 25,
        fontWeight : 'bold',
        margin : 5,
        color : '#48BBEC'
    },
    title : {
        fontSize : 20,
        margin : 5,
        color : '#656565'
    },
    description : {
        fontSize : 18,
        margin : 5,
        color : '#656565'
    }

})