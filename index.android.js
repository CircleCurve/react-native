/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

import { StackNavigator } from 'react-navigation';
import {SearchPage} from './SearchPage';
import {SearchResult} from './SearchResult'; 
import {PropertyView} from './PropertyView'; 


class PropertyFinderApp extends Component {
  static navigationOptions = {
    title : 'Property Finder'
  }
  
  render() {
    return (
        <View style={ styles.container}>
          <Text style={styles.text}>
            Hello navigation
          </Text>
        </View>
    );
  }
}

const SimpleApp = StackNavigator({
  //test : { screen : PropertyFinderApp },
  searchPage: { screen : SearchPage },
  searchResult : {screen : SearchResult},
  propertyView : {screen : PropertyView}
}); 

const styles = StyleSheet.create({
  text : {
    color : "black",
    backgroundColor : "white",
    fontSize : 20, 
  },
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
});

AppRegistry.registerComponent('Test', () => SimpleApp);
